# Setup chaincode in chaincode/property-app/go dir

>       go mod init example.com/property
>        go get
>        go build

# cd to test-network dir

# Start Test Network:

>       ./network.sh up -ca
>	    ./network.sh createChannel 
This will create two channels called channel1 and channel2
channel1 : peer0org1, peer0org2
channel2 : peer1org1, peer2org2

# Set Network ENV VARS

>        export PATH=${PWD}/../bin:$PATH
>	     export FABRIC_CFG_PATH=$PWD/../config/
>        export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem


# Package Chaincode:

>    peer lifecycle chaincode package property.tar.gz --path ../chaincode/property-app/go/ --lang golang --label property_1


# Setup channel 1
# Install on Org1 peer0:

    SETENV:
>       export CORE_PEER_TLS_ENABLED=true
>		export CORE_PEER_LOCALMSPID="Org1MSP"
>		export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
>		export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
>		export CORE_PEER_ADDRESS=localhost:7051
    
    Install:
>		peer lifecycle chaincode install property.tar.gz --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE

# Approve on Org1 peer0:

BE SURE TO UPDATE PROPERTY ID IN COMMAND BELOW!
    Approve:

>        peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --sequence 1 --cafile $ORDERER_CA --channelID channel1 --name property --version 1.0 --init-required --package-id property_1:653bb18d1d7af8ab1b015f1a17791b60e6e9780586e4b6342effadd3b7a7abfe 


# Install on Org2 peer0:

    SETENV:
>       export CORE_PEER_TLS_ENABLED=true
>		export CORE_PEER_LOCALMSPID="Org2MSP"
>		export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
>		export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
>		export CORE_PEER_ADDRESS=localhost:11051
    
    Install:
>		peer lifecycle chaincode install property.tar.gz --peerAddresses localhost:11051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE

# Approve on Org2 peer0:
BE SURE TO UPDATE PROPERTY ID IN COMMAND BELOW!
    Approve:

>        peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --sequence 1 --cafile $ORDERER_CA --channelID channel1 --name property --version 1.0 --init-required --package-id property_1:653bb18d1d7af8ab1b015f1a17791b60e6e9780586e4b6342effadd3b7a7abfe 

# Commit Chaincode:
    SET ENV:

>		export CORE_PEER_LOCALMSPID="Org1MSP"
>    	export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
>		export CORE_PEER_TLS_ROOTCERT_FILE_ORG1=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
>		export CORE_PEER_TLS_ROOTCERT_FILE_ORG2=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
>    	export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
>    	export CORE_PEER_ADDRESS=localhost:7051
>		export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

    Commit Chaincode    
>        peer lifecycle chaincode commit -o  localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA --channelID channel1 --name property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:11051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 --version 1.0 --sequence 1 --init-required

# Invoke Chaincode:
    	
>        peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel1 -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:11051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 --isInit -c '{"Args":[]}'





# Setup Channel2

# Install on Org1 peer1:

    SETENV:
>       export CORE_PEER_TLS_ENABLED=true
>		export CORE_PEER_LOCALMSPID="Org1MSP"
>		export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt
>		export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
>		export CORE_PEER_ADDRESS=localhost:9051
    
    Install:

>        peer lifecycle chaincode install property.tar.gz --peerAddresses localhost:9051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE

# Approve on Org2 peer1:
BE SURE TO UPDATE PROPERTY ID IN COMMAND BELOW!
    Approve:

>        peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --sequence 1 --cafile $ORDERER_CA --channelID channel2 --name property --version 1.0 --init-required --package-id property_1:653bb18d1d7af8ab1b015f1a17791b60e6e9780586e4b6342effadd3b7a7abfe 


# Install on Org2 peer1:

    SETENV:
>       export CORE_PEER_TLS_ENABLED=true
>		export CORE_PEER_LOCALMSPID="Org2MSP"
>		export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/ca.crt
>		export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
>		export CORE_PEER_ADDRESS=localhost:13051
    
    Install:

>        peer lifecycle chaincode install property.tar.gz --peerAddresses localhost:13051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE

# Approve on Org2 peer1:
BE SURE TO UPDATE PROPERTY ID IN COMMAND BELOW!
    Approve:

>        peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --sequence 1 --cafile $ORDERER_CA --channelID channel2 --name property --version 1.0 --init-required --package-id property_1:653bb18d1d7af8ab1b015f1a17791b60e6e9780586e4b6342effadd3b7a7abfe 


# Commit Chaincode:
    SET ENV:

>		export CORE_PEER_LOCALMSPID="Org1MSP"
>    	export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt
>		export CORE_PEER_TLS_ROOTCERT_FILE_ORG1=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer1.org1.example.com/tls/ca.crt
>		export CORE_PEER_TLS_ROOTCERT_FILE_ORG2=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer1.org2.example.com/tls/ca.crt
>    	export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
>    	export CORE_PEER_ADDRESS=localhost:9051
>		export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

    Commit Chaincode    
>        peer lifecycle chaincode commit -o  localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA --channelID channel2 --name property --peerAddresses localhost:9051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:13051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 --version 1.0 --sequence 1 --init-required

# Invoke Chaincode:
    	
>        peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel2 -n property --peerAddresses localhost:9051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:13051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 --isInit -c '{"Args":[]}'



# QUERY COMMITED CHAINCODE ON Channel 2
		//Add property
		
>       peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel2 -n property --peerAddresses localhost:9051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:13051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["AddProperty", "1", "113 Main St", "1000", "John","115000"]}'

>		peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel2 -n property --peerAddresses localhost:9051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:13051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["AddProperty", "2", "286 Meadow Street", "6000", "Paul Jones","250000"]}'

		//Query property by ID
>		peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel2 -n property --peerAddresses localhost:9051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:13051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["QueryPropertyByID", "1"]}'

		//Query All Properties
>		peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel2 -n property --peerAddresses localhost:13051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["QueryAllProperties"]}'


# CHANGE BACK TO CHANNEL 1
>		export CORE_PEER_LOCALMSPID="Org1MSP"
>    	export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
>		export CORE_PEER_TLS_ROOTCERT_FILE_ORG1=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
>		export CORE_PEER_TLS_ROOTCERT_FILE_ORG2=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
>    	export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
>    	export CORE_PEER_ADDRESS=localhost:7051
>		export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

# QUERY COMMITED CHAINCODE ON Channel 1

		//Add property
		
>       peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel1 -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:11051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["AddProperty", "1", "113 South St", "10450", "Mere","330000"]}'

>		peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel1 -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:11051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["AddProperty", "2", "286 Forest Street", "60000", "Kali","750000"]}'

		//Query property by ID
>		peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel1 -n property --peerAddresses localhost:7051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG1 --peerAddresses localhost:11051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["QueryPropertyByID", "1"]}'

		//Query All Properties
>		peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C channel1 -n property --peerAddresses localhost:11051 --tlsRootCertFiles $CORE_PEER_TLS_ROOTCERT_FILE_ORG2 -c '{"Args":["QueryAllProperties"]}'

compare the query all properties on channel1 vs channel2


# Shutdown Network:
>        ./network.sh down

# View a peer's channel membership
    set ENV VARs for the peer in question
>    peer channel list